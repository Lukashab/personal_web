import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const LOGIN = 'LOGIN'
const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
const LOGOUT = 'LOGOUT'

export default new Vuex.Store({
  strict: true,
  state: {
    isUserLoggedIn: !!localStorage.getItem('token')
  },
  mutations: {
    [LOGIN] (state) {
      state.pending = true
    },
    [LOGIN_SUCCESS] (state) {
      state.isUserLoggedIn = true
      state.pending = false
    },
    [LOGOUT] (state) {
      state.isUserLoggedIn = false
    }
  },
  actions: {
    login ({commit}, token) {
      commit(LOGIN)
      return new Promise(resolve => {
        setTimeout(() => {
          localStorage.setItem('token', token)
          resolve()
          commit(LOGIN_SUCCESS)
        }, 1000)
      })
    },
    logout ({commit}) {
      localStorage.removeItem('token')
      commit(LOGOUT)
    }
  },
  getters: {
    isUserLoggedIn: state => {
      return state.isUserLoggedIn
    }
  }
})
