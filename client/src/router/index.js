
import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Admin from '@/components/admin/Layout'
import Register from '@/components/admin/user/Register'
import Login from '@/components/admin/user/Login'
import Skills from '@/components/admin/skills/List'
import SkillEdit from '@/components/admin/skills/Edit'
import Tags from '@/components/admin/tags/List'
import TagEdit from '@/components/admin/tags/Edit'
import Jobs from '@/components/admin/jobs/List'
import JobEdit from '@/components/admin/jobs/Edit'
import Schools from '@/components/admin/schools/List'
import SchoolEdit from '@/components/admin/schools/Edit'

Vue.use(Router)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/admin',
    name: 'Admin',
    component: Admin,
    meta: {
      redirect: true,
      routes: {
        user: '/admin/skills',
        resident: '/admin/login'
      }
    },
    children: [
      {
        path: 'login',
        component: Login
      },
      {
        path: 'register',
        component: Register
      },
      {
        path: 'skills',
        component: Skills,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'skills/edit',
        component: SkillEdit,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'skills/edit/:id',
        component: SkillEdit,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'tags',
        component: Tags,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'tags/edit',
        component: TagEdit,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'tags/edit/:id',
        component: TagEdit,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'jobs',
        component: Jobs,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'jobs/edit',
        component: JobEdit,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'jobs/edit/:id',
        component: JobEdit,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'schools',
        component: Schools,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'schools/edit',
        component: SchoolEdit,
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'schools/edit/:id',
        component: SchoolEdit,
        meta: {
          requiresAuth: true
        }
      }
    ]
  }
]

const router = new Router({routes, mode: 'history'})

router.beforeEach((to, from, next) => {
  const isUserLoggedIn = localStorage.getItem('token')

  // authorization
  if (to.meta.requiresAuth) {
    if (!isUserLoggedIn) {
      next({path: '/admin/login'})
    } else {
      next()
    }
  }

  // redirect if necessary
  if (to.meta.routes) {
    const redirectRoute = isUserLoggedIn ? to.meta.routes.user : to.meta.routes.resident

    if (redirectRoute) {
      next({path: redirectRoute})
    }
  }

  next()
})

export default router
