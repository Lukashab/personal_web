import Api from '@/services/Api'
import AuthenticationService from '@/services/AuthenticationService'

export default {
  index () {
    return Api().get('api/tags', AuthenticationService.getTokenHeader())
  },
  getByName (name) {
    return Api().get('api/tags/search/' + name, AuthenticationService.getTokenHeader())
  },
  getById (id) {
    return Api().get('api/tags/' + id, AuthenticationService.getTokenHeader())
  },
  post (tag) {
    return Api().post('api/tags', tag, AuthenticationService.getTokenHeader())
  },
  update (id, tag) {
    return Api().post('api/tags/' + id, tag, AuthenticationService.getTokenHeader())
  },
  delete (id) {
    return Api().delete('api/tags/' + id, AuthenticationService.getTokenHeader())
  }
}
