import Api from '@/services/Api'
import AuthenticationService from '@/services/AuthenticationService'

export default {
  index () {
    return Api().get('api/jobs', AuthenticationService.getTokenHeader())
  },
  getById (id) {
    return Api().get('api/jobs/' + id, AuthenticationService.getTokenHeader())
  },
  getByName (name) {
    return Api().get('api/jobs/search/' + name, AuthenticationService.getTokenHeader())
  },
  getPreparedDescriptions () {
    return Api().get('api/jobs/descriptions/new', AuthenticationService.getTokenHeader())
  },
  post (job) {
    return Api().post('api/jobs', job, AuthenticationService.getTokenHeader())
  },
  update (id, job) {
    return Api().post('api/jobs/' + id, job, AuthenticationService.getTokenHeader())
  },
  delete (id) {
    return Api().delete('api/jobs/' + id, AuthenticationService.getTokenHeader())
  }

}
