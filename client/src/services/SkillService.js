import Api from '@/services/Api'
import AuthenticationService from '@/services/AuthenticationService'

export default {
  index () {
    return Api().get('api/skills', AuthenticationService.getTokenHeader())
  },
  getByName (name) {
    return Api().get('api/skills/search/' + name, AuthenticationService.getTokenHeader())
  },
  getById (id) {
    return Api().get('api/skills/' + id, AuthenticationService.getTokenHeader())
  },
  getPreparedDescriptions () {
    return Api().get('api/skills/descriptions/new', AuthenticationService.getTokenHeader())
  },
  post (skill) {
    return Api().post('api/skills', skill, AuthenticationService.getTokenHeader())
  },
  update (id, skill) {
    return Api().post('api/skills/' + id, skill, AuthenticationService.getTokenHeader())
  },
  delete (id) {
    return Api().delete('api/skills/' + id, AuthenticationService.getTokenHeader())
  }
}
