import Api from '@/services/Api'
import AuthenticationService from '@/services/AuthenticationService'

export default {
  index () {
    return Api().get('api/schools', AuthenticationService.getTokenHeader())
  },
  getById (id) {
    return Api().get('api/schools/' + id, AuthenticationService.getTokenHeader())
  },
  getByName (name) {
    return Api().get('api/schools/search/' + name, AuthenticationService.getTokenHeader())
  },
  getPreparedDescriptions () {
    return Api().get('api/schools/descriptions/new', AuthenticationService.getTokenHeader())
  },
  post (school) {
    return Api().post('api/schools', school, AuthenticationService.getTokenHeader())
  },
  update (id, school) {
    return Api().post('api/schools/' + id, school, AuthenticationService.getTokenHeader())
  },
  delete (id) {
    return Api().delete('api/schools/' + id, AuthenticationService.getTokenHeader())
  }

}
