
module.exports = (sequelize, DataTypes) => {
    const Skill = sequelize.define('Skill', {
        name: {
            type: DataTypes.STRING,
            unique: true,
        },
        rank: {
            type: DataTypes.INTEGER,
        },
    });

    return Skill;
};
