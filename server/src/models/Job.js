
module.exports = (sequelize, DataTypes) => {
    const Job = sequelize.define('Job', {
        name: {
            type: DataTypes.STRING,
            unique: true,
        },
        company: {
            type: DataTypes.STRING,
        },
        location: {
            type: DataTypes.STRING,
        },
        from: {
            type: DataTypes.DATE,
        },
        to: {
            type: DataTypes.DATE,
        }
    });

    return Job;
};