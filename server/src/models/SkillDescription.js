
module.exports = (sequelize, DataTypes) => {
    const SkillDescription = sequelize.define('SkillDescription', {
        text: {
            type: DataTypes.STRING,
        },
        locale: {
            type: DataTypes.STRING,
        },
    });

    return SkillDescription;
};