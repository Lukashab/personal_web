const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const config = require('../config/config');
const db = {};

const sequelize = new Sequelize(
    config.db.database,
    config.db.user,
    config.db.password,
    config.db.options
);

fs
    .readdirSync(__dirname)
    .filter((file) =>
        file !== 'index.js'
    )
    .forEach((file) => {
        const model = sequelize.import(path.join(__dirname, file));
        db[model.name] = model;
    });

//relations
//Skill - Description
db['Skill'].SkillDescriptions = db['Skill'].hasMany(db['SkillDescription']);
//Skill - tag
db['Skill'].Tags = db['Skill'].belongsToMany(db['Tag'], {through: 'SkillTag'});
db['Tag'].Skills = db['Tag'].belongsToMany(db['Skill'], {through: 'SkillTag'});
//Skill - Job
db['Skill'].Jobs = db['Skill'].belongsToMany(db['Job'], {through: 'SkillJob'});
db['Job'].Skills = db['Job'].belongsToMany(db['Skill'], {through: 'SkillJob'});
//Skill - School
db['Skill'].Schools = db['Skill'].belongsToMany(db['School'], {through: 'SkillSchool'});
db['School'].Skills = db['School'].belongsToMany(db['Skill'], {through: 'SkillSchool'});
//Job - Description
db['Job'].JobDescriptions = db['Job'].hasMany(db['JobDescription']);
//School - Description
db['School'].SchoolDescriptions = db['School'].hasMany(db['SchoolDescription']);

db.sequelize = sequelize;

module.exports = db;