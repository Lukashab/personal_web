
module.exports = (sequelize, DataTypes) => {
    const JobDescription = sequelize.define('JobDescription', {
        text: {
            type: DataTypes.STRING,
        },
        locale: {
            type: DataTypes.STRING,
        },
    });

    return JobDescription;
};