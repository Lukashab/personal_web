
module.exports = (sequelize, DataTypes) => {
    const School = sequelize.define('School', {
        name: {
            type: DataTypes.STRING,
        },
        degree: {
            type: DataTypes.ENUM,
            values: ['bachelor' ,'master'],
        },
        location: {
            type: DataTypes.STRING,
        },
        from: {
            type: DataTypes.DATE,
        },
        to: {
            type: DataTypes.DATE
        },
    });

    return School;
};