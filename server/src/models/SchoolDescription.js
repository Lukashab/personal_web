
module.exports = (sequelize, DataTypes) => {
    const SchoolDescription = sequelize.define('SchoolDescription', {
        text: {
            type: DataTypes.STRING,
        },
        locale: {
            type: DataTypes.STRING,
        },
    });

    return SchoolDescription;
};