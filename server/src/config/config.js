module.exports = {
    port: process.env.PORT || 8081,
    db: {
        database: process.env.DB_NAME || 'personal_web',
        user: process.env.DB_USER || 'personal_web',
        password: process.env.DB_PASS || 'personal_web',
        options: {
            dialect: process.env.DIALECT || 'sqlite',
            host: process.env.HOST || 'localhost',
            storage: './personal_web.sqlite'
        }
    },
    authentication: {
        jwtSecret: process.env.JWT_SECRET || 'secret',
    },
    languages: [
        'cs',
        'en',
        'de'
    ]
};