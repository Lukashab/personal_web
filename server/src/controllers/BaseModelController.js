const Express = require('express');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
class BaseModelController {

    /**
     * BaseModelController constructor
     * @param model
     * @param key
     */
    constructor(model, key) {
        this.router = Express.Router();
        this.model = model;
        this.modelName = model.getTableName().toLowerCase();
        this.key = key;
        //used for defining associations when creating / updating model
        this.includes = {};
    }

    setIncludes(includes) {
        this.includes = {
            include: includes
        }
    }

    create(data) {
        return this.model
            .create(data, this.includes)
            .then((modelInstance) => {
                return modelInstance;
            });
    }

    read(id) {
        return this.model
            .findById(id, this.includes)
            .then((modelInstance) =>{
                return modelInstance;
            });
    }

    update(id, data) {

        return this.model
            .update(data, {
                where: {
                    id: id,
                }
            })
            .then((modelInstance) => {
                return modelInstance;
            });
    }

    delete(id) {
        return this.model
            .destroy({
                where: {
                    id: id
                }
            })
            .then(() => {
                return {};
            })
    }

    list() {
        return this.model
            .findAll(this.includes)
            .then((modelInstance) => {
                return modelInstance
            });
    }

    listByName(name) {
        return this.model.
        findAll({
            where: {
                name: {
                    [Op.like]: '%' + name + '%'
                }
            }
        })
            .then((modelInstance) => {
                return modelInstance;
            });
    }

    route() {

        this.router.get("/", async (req, res) => {
            try {
                let response = await this.list();
                res.send(response);
            } catch (err) {
                res.status(500).send({
                    error: "Error occured while asking for " + this.modelName + " list"
                })
            }
        });

        this.router.get("/:id", async (req, res) => {
            try {
                let id = req.params.id;
                let response = await this.read(id);
                res.send(response);
            } catch (err) {
                res.status(500).send({
                    error: "Error occured while asking for " + this.modelName + " with id = " + id
                })
            }
        });

        this.router.get("/search/:name", async (req, res) => {
            try {
                let name = req.params.name;
                console.log(name);
                let response = await this.listByName(name);
                res.send(response);
            } catch (err) {
                res.status(500).send({
                    error: "Error occured while asking for " + this.modelName + " with name constisting '" + name + "'substring",
                })
            }
        });

        this.router.post("/", async (req, res) => {
            try {
                let response = await this.create(req.body);
                res.send(response);
            } catch (error) {
                res.status(500).send({
                    error: "Error occured while creating new " + this.modelName + " : " + error
                })
            }

        });

        this.router.post("/:id", async (req, res) => {
            try {
                let id = req.params.id;
                let response = await this.update(id, req.body)    ;
                res.send(response);
            } catch (error) {
                console.log(error)
                res.status(500).send({
                    error: "Error occured while updating " + this.modelName + " with id = " + id
                })
            }

        });

        this.router.delete("/:id", async (req, res) => {
            try {
                const id = req.params.id;
                let response = await this.delete(id);
                res.send(response);
            } catch (error) {
                res.status(500).send({
                    error: "Error occured while deleting " + this.modelName + " with id = " + id
                })
            }

        });

        return this.router;
    }

}

module.exports = BaseModelController;