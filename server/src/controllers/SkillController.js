const BaseModelController = require('./BaseModelController');
const {Skill} = require('../models');
const config = require('../config/config')

class SkillController extends BaseModelController {

    constructor() {
        super(Skill, 'id');

        this.setIncludes([
            Skill.SkillDescriptions,
            Skill.Tags,
            Skill.Jobs,
            Skill.Schools
        ])
    }


    create(data) {
        return this.model
            .create(data, [Skill.SkillDescriptions])
            .then((modelInstance) => {
                modelInstance.setTags(data.Tags);
                modelInstance.setJobs(data.Jobs);
                modelInstance.setSchools(data.Schools);

                return modelInstance;
            });
    }

    update(id, data) {

        this.model
            .update(data, {
                where: {
                    id: id,
                }
            })

        this.model
            .findById(id)
            .then((modelInstance) =>{
                modelInstance.setTags(data.Tags);
                modelInstance.setJobs(data.Jobs);
                modelInstance.setSchools(data.Schools);
                return modelInstance;
            });
    }


    route() {
        super.route();

        this.router.get("/descriptions/new", async (req, res) => {
            try {
                let descriptions = [];
                for (let language of config.languages) {
                    descriptions.push({
                        locale: language
                    });
                }
                res.send(descriptions);
            } catch (err) {
                res.status(500).send({
                    error: "Error occured while asking for prepared " + Skill.JobDescriptions
                })
            }
        });

        return this.router;
    }
}

module.exports = SkillController;