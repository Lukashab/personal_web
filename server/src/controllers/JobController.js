const BaseModelController = require('./BaseModelController');
const {Job} = require('../models');
const config = require('../config/config');

class JobController extends BaseModelController {

    constructor() {
        super(Job, 'id');
        this.setIncludes([
            Job.JobDescriptions,
        ])
    }

    route() {
        super.route();

        this.router.get("/descriptions/new", async (req, res) => {
            try {
                let descriptions = [];
                for (let language of config.languages) {
                    descriptions.push({
                        locale: language
                    });
                }
                res.send(descriptions);
            } catch (err) {
                res.status(500).send({
                    error: "Error occured while asking for prepared " + Job.JobDescriptions
                })
            }
        });

        return this.router;
    }
    
}

module.exports = JobController;