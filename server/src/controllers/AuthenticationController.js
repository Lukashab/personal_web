
const {User} = require('../models');
const jwt = require('jsonwebtoken');
const config = require('./../config/config');


function jwtSignUser (user) {
    const ONE_WEEK = 60 * 60 * 24 * 7;
    return jwt.sign(user, config.authentication.jwtSecret, {
        expiresIn: ONE_WEEK,
    })
}


module.exports = {
     async register (req, res) {
        try {
            const user = await User.create(req.body);
            res.status(200).send();
        } catch (err) {
            res.status(400).send({
                error: 'Error in registration - ' + err.toString(),
            })
        }
    },
    async login (req, res) {
        try {
            const {email, password} = req.body;
            const user = await User.findOne({
                where: {
                    email: email,
                }
            });

            const isPasswordValid = user.comparePassword(password);

            if (!user || !isPasswordValid) {
                res.status(403).send({
                    error: 'The login information was incorrect',
                })
            }

            const userJson = user.toJSON();

            res.send({
                user: userJson,
                token: jwtSignUser(userJson),
            });
        } catch (err) {
            res.status(500).send({
                error: 'An error has occured during the login process'
            })
        }
    }
};