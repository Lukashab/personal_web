const BaseModelController = require('./BaseModelController');
const {School} = require('../models');
const config = require('../config/config');

class SchoolController extends BaseModelController {

    constructor() {
        super(School, 'id');
        this.setIncludes([
            School.SchoolDescriptions
        ])
    }


    route() {
        super.route();

        this.router.get("/descriptions/new", async (req, res) => {
            try {
                let descriptions = [];
                for (let language of config.languages) {
                    descriptions.push({
                        locale: language
                    });
                }
                res.send(descriptions);
            } catch (err) {
                res.status(500).send({
                    error: "Error occured while asking for prepared " + School.JobDescriptions
                })
            }
        });

        return this.router;
    }
}

module.exports = SchoolController;