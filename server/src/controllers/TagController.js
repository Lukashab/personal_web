const BaseModelController = require('./BaseModelController');
const {Tag} = require('../models');

class TagController extends BaseModelController {

    constructor() {
        super(Tag, 'id');
    }
}

module.exports = TagController;