
const AuthenticationController = require('./controllers/AuthenticationController');
const AuthenticationControllerPolicy = require('./policies/AuthenticationControllerPolicy');
const SkillController = require('./controllers/SkillController');
const TagController = require('./controllers/TagController');
const SchoolController = require('./controllers/SchoolController');
const JobController = require('./controllers/JobController');


module.exports = (app) => {
    //authentication
    app.post('/register',
        AuthenticationControllerPolicy.register,
        AuthenticationController.register
    );
    app.post('/login',
        AuthenticationController.login
    );

    //model routes
    app.use('/api/skills', AuthenticationControllerPolicy.authenticate, new SkillController().route());
    app.use('/api/tags', AuthenticationControllerPolicy.authenticate, new TagController().route());
    app.use('/api/schools', AuthenticationControllerPolicy.authenticate, new SchoolController().route());
    app.use('/api/jobs', AuthenticationControllerPolicy.authenticate, new JobController().route());

};