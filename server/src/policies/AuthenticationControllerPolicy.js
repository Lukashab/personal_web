const Joi = require('joi');
const jwt = require('jsonwebtoken');
const config = require('../config/config');


module.exports = {
    register (req, res, next) {
        const schema = {
            email: Joi.string(),
            password: Joi.string().regex(
                new RegExp('^[a-zA-Z0-9]{8,32}$')
            )
        };

        const {error, value} = Joi.validate(req.body, schema);

        if (error) {
            switch(error.details[0].context.key) {
                case 'email':
                    res.status(400).send({
                        error: "You must provide a valid email"
                    });
                    break;
                case 'password':
                    res.status(400).send({
                        error: "The password provided failed to match the rules (^[a-zA-Z0-9]{8,32}$)"
                    });
                    break;
                default:
                    res.status(400).send({
                        error: "Invalid registration information"
                    });
                    break
            }
        } else {
            next();
        }
    },

    authenticate (req, res, next) {
        const token = req.body.token || req.query.token || req.headers['x-access-token'];
        if (token) {
            jwt.verify(token, config.authentication.jwtSecret, function(err, decoded) {
                if (err) {
                    res.status(403).send({
                        error: "Failed to authenticate token."
                    });
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;
                    next();
                }
            });
        } else {
            res.status(403).send({
                error: "You do not have permission to access this route."
            })
        }
    }
};